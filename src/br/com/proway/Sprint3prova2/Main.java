package br.com.proway.Sprint3prova2;

import br.com.proway.Sprint3prova2.model.BestSeller;
import br.com.proway.Sprint3prova2.model.Biblioteca;
import br.com.proway.Sprint3prova2.model.Recomendados;
import br.com.proway.Sprint3prova2.utils.DiasSemana;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        Biblioteca bibli = new Biblioteca();
        DiasSemana dia;

        bibli.inserirLivro(new BestSeller(1, "O inferno de Dante", "Sátira", 1023400));
        bibli.inserirLivro(new Recomendados(2, "Dom Quixote", "Filosofia", 4.5));
        bibli.inserirLivro(new BestSeller(3, "Harry Potter e a Pedra Filosofal", "Ficção", 14400));
        bibli.inserirLivro(new Recomendados(4, "A metamorfose", "Filosofia", 4.9));
        bibli.inserirLivro(new Recomendados(5, "Senhor dos Anéis", "Conto", 4.4));

        while (true) {
            String texto = "                     Sistema Biblioteca\n \n";
            for (int i = 0; i < bibli.getSistema().size(); i++) {
                texto += bibli.getSistema().get(i).getId() + " Título: " + bibli.getSistema().get(i).getTitulo() + ".  Tema: " +
                        bibli.getSistema().get(i).getTema() + "\n";
            }


            String[] opcoes = {"Locar", "Sair"};

            int opcao = JOptionPane.showOptionDialog(null, texto, "Biblioteca", JOptionPane.DEFAULT_OPTION,
                    JOptionPane.INFORMATION_MESSAGE, null, opcoes, opcoes[0]);
            if (opcao == 0) {
                dia = DiasSemana.valueOf(JOptionPane.showInputDialog(null, "Informe o dia da semana").toString().toUpperCase());

                if (false == bibli.BlibliotecaFechada(DiasSemana.valueOf(dia.toString()))) {
                    break;
                }
                int id = Integer.parseInt(JOptionPane.showInputDialog(null, "Informe o id do livro que deseja locar"));
                boolean verdade = bibli.getSistema().stream().anyMatch(l -> l.getId() == id);
                if (verdade == true) {
                    bibli.locarLivro(id);
                } else {
                    JOptionPane.showMessageDialog(null, "O livro informado não existe");
                }

            }
            if (opcao == -1 || opcao == 1) {
                JOptionPane.showMessageDialog(null, "         Muito obrigado! \n" + "Aguardamos seu retorno...");
                break;
            }

        }
    }
}
