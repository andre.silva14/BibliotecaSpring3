package br.com.proway.Sprint3prova2.model;

public class Recomendados extends Livro {
    private double avalicao;

    public Recomendados(int id, String titulo, String tema, double avaliacao) {
        super(id, titulo, tema);
        this.avalicao = avaliacao;
    }
}
