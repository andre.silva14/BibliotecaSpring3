package br.com.proway.Sprint3prova2.model;

public class BestSeller extends Livro{
    private int vendidos;

    public BestSeller(int id, String autor, String tema, int vendidos) {
        super(id, autor, tema);
        this.vendidos = vendidos;
    }
}
