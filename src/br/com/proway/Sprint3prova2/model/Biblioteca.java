package br.com.proway.Sprint3prova2.model;

import br.com.proway.Sprint3prova2.utils.DiasSemana;

import javax.swing.*;
import java.util.ArrayList;

public class Biblioteca implements Indisponivel {
    private  ArrayList<Livro> sistema = new ArrayList<>();


    public ArrayList<Livro> getSistema() {
        return sistema;
    }

    public void locarLivro(int id) {
        sistema.removeIf(l -> l.getId() == id);
    }

    public void inserirLivro(Livro livro) {
        sistema.add(livro);
    }

    public boolean BlibliotecaFechada(DiasSemana dia) {
        if (dia.getValor() == 1 || dia.getValor() == 7) {
            JOptionPane.showMessageDialog(null, "A bliblioteca está fechada! \n" +
                    "   Retorne outro dia...");
            return false;
        } else {
            return true;
        }
    }


}




