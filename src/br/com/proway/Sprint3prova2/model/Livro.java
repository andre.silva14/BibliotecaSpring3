package br.com.proway.Sprint3prova2.model;

public abstract class Livro {
    private int id;
    private String titulo;
    private String tema;

    public Livro(int id, String titulo, String tema) {
        this.id = id;
        this.titulo = titulo;
        this.tema = tema;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getTema() {
        return tema;
    }


    public int getId() {
        return id;
    }
}
