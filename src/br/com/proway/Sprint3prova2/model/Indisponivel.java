package br.com.proway.Sprint3prova2.model;

import br.com.proway.Sprint3prova2.utils.DiasSemana;

import javax.swing.*;

public interface Indisponivel {
    default boolean BlibliotecaFechada(DiasSemana dia) {
        if (dia.getValor() == 1 || dia.getValor() == 7) {
            JOptionPane.showMessageDialog(null, "A bliblioteca está fechada! \n" +
                    "   Retorne outro dia...");
            return false;
        } else {
            return true;
        }

    }
}
